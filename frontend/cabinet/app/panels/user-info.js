import React from 'react'
import Coin from '../help/coin'
import { toMoney } from '../help/formatter'
import { connect } from 'react-redux'
import { isUndefined } from 'util'
import { load } from '../store/data-loader'
import constant from '../store/constants'

class UserInfo extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            isLoad: true
        }
    }
    componentWillMount() {
        if (isUndefined(this.props.userInfo)) {
            load(constant.USER_INFO_URL).then(response => {            
                this.props.dispatch({
                    type: constant.SET_DATA,
                    state: {userInfo: response.data['response']}
                })
                this.setState({isLoad: false})          
            })   
        }
        else {
            this.setState({isLoad: false})
        }        
    }
    render() {
        if (this.state.isLoad) {
            return <Loader />
        }
        const avatarPath = this.props.userInfo.get('avatar')
        const email = this.props.userInfo.get('email')
        const balance = this.props.userInfo.get('balance')
        const totalIncome = this.props.userInfo.get('total_balance').get('total_balance')
        return(            
            <div className="user-info">
                <img className="avatar" src={avatarPath} alt="avatar" />
                <div className="short-info">
                    <p className="email">{email}</p>
                    <p className="balance">БАЛАНС:&nbsp;<span className="balance-value">{toMoney(balance)}</span><Coin isBlack={true} /></p>
                </div>
                <p className="all-time">ЗА ВСЕ ВРЕМЯ ЗАРАБОТАНО:&nbsp;<span className="all-time-value">{toMoney(totalIncome)}</span><Coin isBlack={true} /></p>
            </div>            
        )
    }
}

const Loader = () => {
    return(
        <div className='user-info-loader'>
            <img src='/images/loader.png' />
        </div>
    )
}

function mapStateToProps(state) {
    return {
        userInfo: state.get('userInfo')
    }
}

module.exports = connect(mapStateToProps)(UserInfo)