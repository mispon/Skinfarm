import React from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { isUndefined } from 'util';

const admins = [
    'ruslan-cool@live.ru',
    'randowgm@gmail.com',
    'dontranes@gmail.com',
    'pankratov.b@gmail.com',
    'tomythesupa@gmail.com'
]

class Header extends React.Component {
    constructor(props) {
        super(props)
    }
    isAdmin() {
        if (isUndefined(this.props.userInfo)) {
            return false
        }
        let email = this.props.userInfo.get('email')
        return admins.indexOf(email) != -1
    }
    render() { 
        if (this.isAdmin()) {
            document.getElementById('navigation').style.width = '60%'
        }       
        return(
            <div className="cabinet-header">
                <nav>
                    <ul id="navigation">
                        <li className="history-link"><Link to='history'>ИСТОРИЯ</Link></li>
                        <li className="referals-link"><Link to='referals'>РЕФЕРАЛЬНАЯ СИСТЕМА</Link></li> 
                        {this.isAdmin() && <li className="offers-link"><Link to='offers'>ЗАКАЗЫ</Link></li>}                        
                    </ul>
                </nav>
            </div>
        )        
    }
}

function mapStateToProps(state) {
    return {
        userInfo: state.get('userInfo')
    }
}

module.exports = connect(mapStateToProps)(Header)