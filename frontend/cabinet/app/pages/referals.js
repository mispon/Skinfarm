'use strict'

import React from 'react'
import Coin from '../help/coin'
import Loader from '../help/loader'
import { formatDate } from '../help/formatter'
import { connect } from 'react-redux'
import { isUndefined } from 'util'
import { load } from '../store/data-loader'
import constant from '../store/constants'

class Referals extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            isLoad: true
        }
    }
    componentWillMount() {
        if (isUndefined(this.props.referals)) {
            load(constant.REFERALS_URL).then(response => {            
                this.props.dispatch({
                    type: constant.SET_DATA,
                    state: {referals: response.data['response']}
                })
                this.setState({isLoad: false})          
            })
        }
        else {
            this.setState({isLoad: false})
        }      
    }
    getSummaryBonus() {
        let result = 0
        this.props.referals.map((item) => {
            result += parseInt(item.get('bonus'))
        })
        return result
    }
    render() {
        if (this.state.isLoad || isUndefined(this.props.userInfo)) {
            return (
                <Loader />
            )
        }
        return(
            <div className="referals">
                <div className="referal-info">
                    <h1 className="call-friend">ПОЗОВИ ДРУГА</h1>
                    <h1 className="get-bonus">ПОЛУЧИ БОНУС</h1>
                    <p>
                        С помощью нашей реферальной системы
                        вы сможете увеличить свои доходы и
                        зарабатывать вместе с друзьями!
                    </p>
                    <p className="target">
                        Когда ваш друг достигнет нужного количества
                        поинтов, вы получите награду за него!
                    </p>
                </div>
                <div className="code-info">
                        <p>Ваш реферальный код</p>
                        <p className="code">{this.props.userInfo.get('ref_code')}</p>
                </div>
                <div className="code-income">
                    <p>ЗАРАБОТАНО С РЕФЕРАЛЬНОЙ СИСТЕМЫ</p>
                    <p className="income-value">{this.getSummaryBonus()}<Coin isBlack={true} /></p>
                </div>
                {this.props.userInfo.get('ref_bonus') > 0 && <VipInfo bonus={this.props.userInfo.get('ref_bonus')} />}
                {
                    this.props.referals.size == 0
                        ? <p className="no-referals">У вас нет рефералов</p>
                        : <ReferalTable referals={this.props.referals} />
                }
            </div>
        )
    }
}

const VipInfo = ({bonus}) => {
    return(
        <div className="vip-info">
            <h1 className="vip">VIP-СТАТУС</h1>
            <p>Вы получаете {bonus}% со всех приглашенных пользователей</p>
        </div>
    )
}

class ReferalTable extends React.Component {
    constructor(props) {
        super(props)
    }
    render() {
        return(
            <div className="referals-table">
                <table cellSpacing="0" cellPadding="0">
                    <thead>
                        <tr>
                            <td>РЕФЕРАЛ</td>
                            <td>ЗАРАБОТАНО</td>
                            <td>НАГРАДА</td>
                            <td>ДАТА</td>
                        </tr>
                    </thead>
                    <tbody>
                    {                            
                        this.props.referals
                        .sort((a, b) => new Date(b.get('updated_at')) - new Date(a.get('updated_at')))
                        .map((item, i) =>
                            <tr key={i}>
                                <td key={`col1_${i}`}>{item.get('email')}</td>
                                <td key={`col2_${i}`}>
                                    <div className="coin-block">
                                        {item.get('earned')}/{item.get('required_earned')} <Coin isBlack={false} />
                                    </div>
                                </td>
                                <td key={`col3_${i}`}><div className="coin-block">{item.get('bonus')} <Coin isBlack={false} /></div></td>
                                <td key={`col4_${i}`}>{formatDate(item.get('bonus_at'))}</td>
                            </tr>
                        )
                    }
                    </tbody>
                </table>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        userInfo: state.get('userInfo'),
        referals: state.get('referals')
    }
}

module.exports = connect(mapStateToProps)(Referals)