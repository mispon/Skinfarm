'use strict'

import React from 'react'
import Loader from '../help/loader'
import { formatDate } from '../help/formatter'
import { connect } from 'react-redux'
import * as actions from '../store/actions'
import constants from '../help/constants'
import { isUndefined } from 'util'
import { bindActionCreators } from 'redux'
import { load } from '../store/data-loader'
import constant from '../store/constants'
import axios from 'axios'

class Offers extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            filter: '',
            isLoad: true
        }
    }
    componentWillMount() {
        if (isUndefined(this.props.offers)) {
            load(constant.OFFERS_URL).then(response => {            
                this.props.dispatch({
                    type: constant.SET_DATA,
                    state: {offers: response.data['response']}
                })  
                this.setState({isLoad: false})                     
            })  
        }
        else {
            this.setState({isLoad: false})
        }       
    }
    onCancel(item) {
        this.props.cancelItem(item)
        setOfferStatus(item, 1)
    }
    onAccept(item) {
        this.props.acceptItem(item)
        setOfferStatus(item, 2)
    }
    onDecline(item) {
        this.props.declineItem(item)
        setOfferStatus(item, 3)
    }
    onFilter(status) {
        this.setState({filter: status})
    }
    getIcon(item) {
        let icon = item.get('icon')
        if (icon.startsWith('http')) {
            return icon
        }
        return `http://skinfarm.net/storage/items/${icon}` 
    }
    cropGoodName(item) {
        let name = item.get('item').get('name')
        if (name.length > 30) {
            name = `${name.substring(0, 30)}...`
        }
        return name
    }
    getStatus(item) {
        let statusId = item.get('status_id')
        switch (statusId) {
            case 1:
                return 'ОЖИДАЕТСЯ'
            case 2:
                return 'ВЫДАН'
            case 3:
                return 'ОТКАЗАН'
        }
    }
    render() {
        if (this.state.isLoad) {
            return(
                <Loader />
            )
        }
        return(
            <div className="offers">
                <div className="offers-table">
                    <table cellSpacing="0" cellPadding="0">
                        <thead>
                            <tr>
                                <td>№ ЗАКАЗА</td>
                                <td>ПОЛЬЗОВАТЕЛЬ</td>
                                <td>ПРЕДМЕТ</td>
                                <td>ДАТА</td>
                                <td>ТРЕЙД-ССЫЛКА</td>
                                <td>СТАТУС</td>
                                <td>ДЕЙСТВИЕ</td>
                            </tr>
                        </thead>
                        <tbody>
                        {
                            this.props.offers
                            .filter(e => this.state.filter ? e.get('status_id') == this.state.filter : e)
                            .sort((a, b) => new Date(b.get('updated_at')) - new Date(a.get('updated_at')))
                            .map((item, i) =>
                                <tr key={i}>
                                    <td key={`col1_${i}`}>#{item.get('id')}</td>
                                    {
                                        <td key={`col2_${i}`} className="user-data">
                                            <div>
                                                <img src={item.get('user') ? item.get('user').get('avatar') : '/images/avatar.png'} alt="ava" />
                                                <span>{item.get('user') ? item.get('user').get('nickname') : 'Неизвестен'}</span>
                                            </div>                                        
                                        </td>
                                    }
                                    <td key={`col3_${i}`} className="good-info">
                                        <div>
                                            <img src={this.getIcon(item.get('item'))} alt="good" />
                                            <span title={item.get('item').get('name')}>{this.cropGoodName(item)}</span>
                                        </div>                                        
                                    </td>
                                    <td key={`col4_${i}`}><span>{formatDate(item.get('updated_at'))}</span></td>
                                    <td key={`col5_${i}`}>
                                        <input className="trade-link" value={item.get('user') != null ? item.get('user').get('tradelink') : ''} readOnly />
                                    </td>
                                    <td key={`col6_${i}`}><span>{this.getStatus(item)}</span></td>
                                    <td key={`col7_${i}`}>
                                        {item.get('status_id') == constants.WAITING && <button className="accept" onClick={() => this.onAccept(item)}>ПОДТВЕРДИТЬ</button>}
                                        {item.get('status_id') == constants.WAITING && <button className="decline" onClick={() => this.onDecline(item)}>ОТКАЗАТЬ</button>}
                                        {item.get('status_id') != constants.WAITING && <button className="cancel" onClick={() => this.onCancel(item)}>ОТМЕНИТЬ</button>}
                                    </td>
                                </tr>
                            )
                        }
                        </tbody>
                    </table>
                    <div className="filters">
                        <button onClick={() => this.onFilter('')}>ВСЕ</button>
                        <button onClick={() => this.onFilter(constants.WAITING)}>ОЖИДАЮТСЯ</button>
                        <button onClick={() => this.onFilter(constants.ACCEPTED)}>ПОДТВЕРЖДЕННЫЕ</button>
                        <button onClick={() => this.onFilter(constants.DECLINED)}>ОТКЛОНЕННЫЕ</button>
                    </div>
                </div>
            </div>
        )
    }
}

const setOfferStatus = (item, status) => {
    let token = document.head.querySelector('meta[name="csrf-token"]')
    axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
    axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';    
    axios.post('/api/v1/history/setStatus', {
        order_id: item.get('id'), 
        status_id: status
    })
}

function mapStateToProps(state) {
    return {
        offers: state.get('offers')
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        cancelItem: actions.cancelItem,
        acceptItem: actions.acceptItem, 
        declineItem: actions.declineItem,
        dispatch: dispatch
    }, dispatch)
}

module.exports = connect(mapStateToProps, mapDispatchToProps)(Offers)