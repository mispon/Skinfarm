'use strict'

import React from 'react'
import Coin from '../help/coin'
import Loader from '../help/loader'
import { formatDate } from '../help/formatter'
import { connect } from 'react-redux'
import { isUndefined } from 'util'
import { load } from '../store/data-loader'
import constant from '../store/constants'

class History extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            isLoad: true
        }
    } 
    componentWillMount() {
        if (isUndefined(this.props.history)) {
            load(constant.HISTORY_URL).then(response => {            
                this.props.dispatch({
                    type: constant.SET_DATA,
                    state: {history: response.data['response']}
                })  
                this.setState({isLoad: false})                     
            })  
        }
        else {
            this.setState({isLoad: false})
        }
    }
    render() {
        if (this.state.isLoad) {
            return(
                <Loader />
            )
        }
        return(
            <div className="history">
                {
                    this.props.history.size == 0 
                        ? <p className="no-history">История отсутствует</p> 
                        : <HistoryTable history={this.props.history} />
                }
            </div>
        )
    }
}

class HistoryTable extends React.Component {
    constructor(props) {
        super(props)
    }
    getIcon(item) {
        let icon = item.get('item').get('icon')
        if (icon.startsWith('http')) {
            return icon
        }
        return `http://skinfarm.net/storage/items/${icon}` 
    }
    getStatus(item) {
        let statusId = item.get('status_id')
        switch (statusId) {
            case 1:
                return 'ОЖИДАЕТСЯ'
            case 2:
                return 'ВЫДАН'
            case 3:
                return 'ОТМЕНЕН'
        }
    }
    render() {
        return(
            <div className="history-table">
                <table cellSpacing="0" cellPadding="0">
                    <TableHeader />
                    <tbody>
                    {
                        this.props.history
                        .sort((a, b) => new Date(b.get('updated_at')) - new Date(a.get('updated_at')))
                        .map((elem, i) =>
                            <tr key={i}>
                                <td key={`col1_${i}`}><img className="icon" src={this.getIcon(elem)} alt='item' /></td>
                                <td key={`col2_${i}`}><p style={getStyle(elem)}>{elem.get('item').get('name')}</p></td>
                                <td key={`col3_${i}`}><div className="cost">{elem.get('item').get('cost')}<Coin isBlack={false} /></div></td>
                                <td key={`col4_${i}`}>{formatDate(elem.get('updated_at'))}</td>
                                <td key={`col5_${i}`}>{this.getStatus(elem)}</td>
                            </tr>
                        )
                    }
                    </tbody>
                </table>
            </div>
        )
    }
}

const TableHeader = () => {
    return(
        <thead>
            <tr>
                <td></td>
                <td>НАИМЕНОВАНИЕ</td>
                <td>ЦЕНА</td>
                <td>ДАТА ЗАПРОСА</td>
                <td>СТАТУС</td>
            </tr>
        </thead>
    )
}

const getStyle = (elem) => {
    let item = elem.get('item')
    let color
    let attr = item.get('attributes')
    if (attr == null) {
        return {}
    }
    color = attr.get('quality_color')
    return {
        backgroundImage: `-webkit-gradient(linear, left bottom, right bottom, from(#${color}), to(#${color}))`,
        backgroundRepeat: 'no-repeat',                
        paddingBottom: '1vw',
        backgroundPosition: '0.5vw 0.7vh',
        backgroundSize: '70% 70%'
    }
}

function mapStateToProps(state) {
    return {
        history: state.get('history')
    }
}

module.exports = connect(mapStateToProps)(History)