'use strict'

export const formatDate = (dateValue) => {
    let t = dateValue.split(/[- :]/)
    let parseDate = new Date(
        parseInt(t[0]), parseInt(t[1] - 1), parseInt(t[2]), parseInt(t[3]), parseInt(t[4]), parseInt(t[5])
    )
    let date = new Date(parseDate)
    let dd = date.getDate()
    let mm = date.getMonth() + 1
    let yyyy = date.getFullYear()
    if(dd < 10){
        dd = '0' + dd
    } 
    if(mm < 10){
        mm = '0' + mm
    } 
    return `${dd}.${mm}.${yyyy}`
}

export const toMoney = (money) => {
    return parseFloat(money).toLocaleString('en-US', {maximumFractionDigits: 2})
}