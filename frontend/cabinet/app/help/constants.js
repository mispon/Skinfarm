const WAITING = 1
const ACCEPTED = 2
const DECLINED = 3

module.exports = {WAITING, ACCEPTED, DECLINED}