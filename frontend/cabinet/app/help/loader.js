'use strict'

import React from 'react'

const Loader = () => {
    return(
        <div className='page-loader'>
            <img src='/images/loader.png' />
        </div>
    )
}

export default Loader