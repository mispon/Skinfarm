'use strict'

import React from 'react'

const Coin = ({isBlack}) => {
    let path = isBlack ? '/images/coin.png' : '/images/w-coin.png'
    return(
        <img className="coin" src={path} alt="coin" />
    )
}

export default Coin