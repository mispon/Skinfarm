'use strict'

import constants from '../help/constants'

const cancelItem = (item) => {
    return {
        type: 'UPDATE_ITEM',
        number: item.get('item_id'),
        status: constants.WAITING
    }
}

const acceptItem = (item) => {
    return {
        type: 'UPDATE_ITEM',
        number: item.get('item_id'),
        status: constants.ACCEPTED
    }
}

const declineItem = (item) => {
    return {
        type: 'UPDATE_ITEM',
        number: item.get('item_id'),
        status: constants.DECLINED
    }
}

module.exports = {cancelItem, acceptItem, declineItem}