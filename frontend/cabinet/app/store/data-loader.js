'use strict'

import axios from 'axios'

function load(url) {
    let token = document.head.querySelector('meta[name="csrf-token"]')
    axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
    axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';    
    return axios.get(url)
}

module.exports = {load}