'use strict'

import { Map } from 'immutable'
import constants from '../help/constants'

const Reducer = (state = Map(), action) => {
    switch (action.type) {
        case 'SET_DATA':
            state = state.merge(action.state)
            break
        case 'UPDATE_ITEM':
            state = state.update('offers', (offers) => {
                let result = []
                offers.forEach((e) => {
                    if (e.get('item_id') == action.number) {
                        e = e.set('status_id', action.status)
                    }
                    result.push(e)
                })
                return result
            })
            break
        default:
            return state
    }
    return state   
}

export default Reducer