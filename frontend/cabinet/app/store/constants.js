'use strict'

module.exports = Object.freeze({
    SET_DATA: 'SET_DATA',
    USER_INFO_URL: '/api/v1/account/getProfileInfo',
    HISTORY_URL: '/api/v1/history/get',
    REFERALS_URL: '/api/v1/account/getReferrals',
    OFFERS_URL: '/api/v1/history/getDetailed'
})