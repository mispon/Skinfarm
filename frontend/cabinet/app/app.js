'use strict'

import '../../styles/cabinet.less'

import React from 'react'
import ReactDOM from 'react-dom'
import { Switch, Route } from 'react-router';
import { HashRouter } from 'react-router-dom'
import { Provider } from 'react-redux'
import { createStore } from 'redux'
import reducer from './store/reducer'
import { loadData } from './store/data-loader'

import UserInfo from './panels/user-info'
import Header from './panels/header'
import History from './pages/history'
import Referals from './pages/referals'
import Offers from './pages/offers'

let store = createStore(reducer)

const App = () => (
    <div className="cabinet-app">
        <UserInfo />
        <Header />
        <Switch>
            <Route exact path='/' component={History} />
            <Route path='/history' component={History} />
            <Route path='/referals' component={Referals} />
            <Route path='/offers' component={Offers} />
        </Switch>
    </div>
)

ReactDOM.render((
    <HashRouter>
        <Provider store={store}>
            <App />
        </Provider>
    </HashRouter>
), document.getElementById('app'))