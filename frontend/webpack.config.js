const path = require('path')
const webpack = require('webpack')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const CleanLessPlugin = require("less-plugin-clean-css")

const extractLess = new ExtractTextPlugin({
    filename: 'css/[name].css'
})

let NODE_ENV;
if (process.env.NODE_ENV) {
    NODE_ENV = process.env.NODE_ENV.replace(/^\s+|\s+$/g, "")
}

const config = {
    context: __dirname,
    entry: {
        site: ['./scripts/site.js', './styles/site.less', './styles/main-page.less', './styles/slider.less', './styles/agreement.less', './styles/hamburgers.less'],
        mainPage: './scripts/main-page.js',
        cabinet: ['./cabinet/app/app.js', './styles/cabinet.less'],
        agreement: './scripts/agreement.js'
    },
    output: {
        path: path.resolve(__dirname, 'public'),
        publicPath: 'public/',
        filename: 'js/[name].js',
        library: '[name]',
    },
    devServer: {
        inline: true,
        hot: true,
        contentBase: './'
    },
    module: {
        rules: [{
            test: /\.js$/,
            exclude: [/node_modules/],
            loader: 'babel-loader',
            query: {
                presets: ['react', 'es2015']
            }
        }, {
            test: /\.less$/,
            exclude: [/node_modules/],
            use: ['css-hot-loader'].concat(extractLess.extract({
                fallback: 'style-loader',
                use: [{
                    loader: 'css-loader'
                }, 
                {
                    loader: 'less-loader',
                    options: {
                        plugins: [
                            new CleanLessPlugin({ advanced: true })
                        ]
                    }
                }]
            }))
        }, {
            test: /\.(gif|png|jpe?g|svg)$/i,
            use: [{
                    loader: 'file-loader',
                    options: {
                        name: 'images/[name].[ext]'
                    }
                }, {
                    loader: 'image-webpack-loader',
                    options: {
                        mozjpeg: {
                            progressive: true,
                            quality: 70
                        }
                    }
                },
            ],
        }, {
            test: /\.(eot|svg|ttf|woff|woff2)$/,
            use: {
                loader: 'file-loader',
                options: {
                    name: 'fonts/[name].[ext]'
                }
            },
        }]
    },
    watch: NODE_ENV == 'dev',
    devtool: NODE_ENV == 'dev' && 'eval',
    plugins: [
        new webpack.EnvironmentPlugin(['NODE_ENV']),
        new webpack.optimize.UglifyJsPlugin({
            compress: { warnings: false }
        }),
        extractLess
    ]
}

module.exports = config