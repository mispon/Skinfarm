'use strict';

document.addEventListener('DOMContentLoaded', () => {
    let acceptField = document.querySelector('#accept-box')
    acceptField.onchange = () => {
        let acceptButton = document.querySelector('.accept-agreement')
        if (acceptField.checked) {
            acceptButton.classList.remove('not-active')
        }
        else {
            acceptButton.classList.add('not-active')
        }
    }
})