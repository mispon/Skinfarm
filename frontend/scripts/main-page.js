'use scrict'

let point = 1

let prev = document.querySelector('.prev')
prev.onclick = () => {
    point = point == 1 ? 4 : point - 1;
    setPointChecked()
}

let next = document.querySelector('.next')
next.onclick = () => {
    point = point == 4 ? 1 : point + 1;
    setPointChecked()
}

const setPointChecked = () => {
    let slide = document.querySelector(`#slide${point}`)
    slide.checked = true
}

document.addEventListener('touchstart', handleTouchStart, false)        
document.addEventListener('touchmove', handleTouchMove, false)

let xDown = null                                                      
let yDown = null                                                        

function handleTouchStart(evt) {                                         
    xDown = evt.touches[0].clientX                                     
    yDown = evt.touches[0].clientY                                    
}                                                

function handleTouchMove(evt) {
    if (!xDown || !yDown) {
        return
    }
    let xUp = evt.touches[0].clientX;                                   
    let yUp = evt.touches[0].clientY
    let xDiff = xDown - xUp
    let yDiff = yDown - yUp
    if (Math.abs(xDiff) > Math.abs(yDiff)) {
        if (xDiff > 0) {
            next.click()
        } else {
            prev.click()            
        }                       
    }
    xDown = null
}