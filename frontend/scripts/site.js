'use scrict'

hamburger.onclick = () => {
    let menu = document.querySelector('.navigation')
    let hamburger = document.querySelector('.hamburger')
    let displayStyle = window.getComputedStyle(menu).display
    if (displayStyle === 'none') {
        menu.style.display = 'block'
        hamburger.classList.add('hamburger--collapse')
        hamburger.classList.add('is-active')
        
    }
    else {
        menu.style.display = 'none'
        hamburger.classList.remove('hamburger--collapse')
        hamburger.classList.remove('is-active')
    } 
}

document.querySelector('.navigation').onclick = () => {
    let hamburger = document.querySelector('.hamburger')
    if (window.getComputedStyle(hamburger).display === 'none') {
        return
    }
    hamburger.click()
}